package com.maxo.dep;

/**
 * Hello name!
 *
 */
public class Dep
{
    public static void hello( String name )
    {
        System.out.println( "Hello " + name + "!" );
    }
}
